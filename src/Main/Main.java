/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import Kantor.Kantor;
import Pegawai.Honorer;
import Pegawai.Manager;
import Pegawai.Marketing;
import Pegawai.Pegawai;

/**
 *
 * @author User pc
 */
public class Main {

    public static void main(String[] args) {
        Kantor kantor = new Kantor();
        Manager mgr = new Manager("P.1200", "Edo", 3);
        mgr.setIstri(1);
        mgr.setJamKerja(8);
        mgr.setJumAnak(2);
        kantor.setManager(mgr);

        Pegawai[] pgw = new Pegawai[3];

        Marketing mk = new Marketing("P.1234", "Aji", 1);
        mk.setJamKerja(9);

        Honorer hnr = new Honorer("P.4321", "Bayu", 2);

        Marketing mk1 = new Marketing("P.5678", "Vandi", 2);
        mk1.setJamKerja(10);

        pgw[0] = mk;
        pgw[1] = hnr;
        pgw[2] = mk1;

        kantor.setPegawai(pgw);

        System.out.println("Nama Manager : " + kantor.getManager().getNama());
        System.out.println("Daftar Pegawai");
        System.out.println("==========================================\n"
                + "No  Nip\t\tNama\tStudi\tGaji\n"
                + "------------------------------------------");
        for (int i = 0; i < pgw.length; i++) {
            System.out.print(i + 1 + ".  " + pgw[i].getNip() + "\t" + pgw[i].getNama() + "\t");
            if (pgw[i] instanceof Marketing) {
                if (((Marketing) pgw[i]).isSelesai() == true) {
                    System.out.print("Selesai\t");
                } else if (((Marketing) pgw[i]).isSelesai() == false) {
                    System.out.print("Studi\t");
                }
            } else if (pgw[i] instanceof Manager) {
                if (((Manager) pgw[i]).isSelesai() == true) {
                    System.out.print("Selesai\t");
                } else if (((Manager) pgw[i]).isSelesai() == false) {
                    System.out.print("Studi\t");
                }
            } else {
                System.out.print("-   \t");
            }
            System.out.println(pgw[i].hitungGatot());
        }
        System.out.println("==========================================");
        System.out.println("Jumlalh Gaji Kantor: " + kantor.hitGajiPeg());
    }
}
