/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Kantor;

import Pegawai.Manager;
import Pegawai.Pegawai;

/**
 *
 * @author User pc
 */
public class Kantor {

    private Manager manager;
    private Pegawai[] pegawai;

    public Kantor() {
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public Pegawai[] getPegawai() {
        return pegawai;
    }

    public void setPegawai(Pegawai[] pegawai) {
        this.pegawai = pegawai;
    }

    public double hitGajiPeg() {
        double total = 0;
        for (int i = 0; i < pegawai.length; i++) {
            total = total + pegawai[i].getGajiTotal();
        }
        return total;
    }
}
