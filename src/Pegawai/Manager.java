/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pegawai;

import TugasBelajar.TugasBelajar;

/**
 *
 * @author User pc
 */
public class Manager extends Pegawai implements TugasBelajar{

    private int jumAnak;
    private int jamKerja;
    private int istri;

    public Manager(String nip, String nama, int golongan) {
        super(nip, nama, golongan);
    }
    

    public int getJumAnak() {
        return jumAnak;
    }

    public void setJumAnak(int jumAnak) {
        this.jumAnak = jumAnak;
    }

    public int getJamKerja() {
        return jamKerja;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public int getIstri() {
        return istri;
    }

    public void setIstri(int istri) {
        this.istri = istri;
    }

    public double hitTunjangan() {
        double tunjangan = 0;
        int max = 3;
        if (jumAnak >= 3) {
            tunjangan = 100000 * 3;
        } else if (jumAnak >= 1 || jumAnak <= 3) {
            tunjangan = 100000 * jumAnak;
        }
        return tunjangan;
    }

    public double hitLembur() {
        int lembur = 0;
        if (jamKerja > 8) {
            lembur = (jamKerja - 8) * 50000;
        }
        return lembur;
    }

    @Override
    public double hitungGatot() {
        gajiTotal = hitGajiPokok() + hitTunjangan() + hitLembur();
        return gajiTotal;
    }

    @Override
    public boolean isSelesai() {
        if (golongan > 1) {
            return true;
        } else{
            return false;
        }
    }
}
