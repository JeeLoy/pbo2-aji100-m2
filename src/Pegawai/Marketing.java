/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pegawai;

import TugasBelajar.TugasBelajar;

/**
 *
 * @author User pc
 */
public class Marketing extends Pegawai implements TugasBelajar {

    private int jamKerja;

    public Marketing(String nip, String nama, int golongan) {
        super(nip, nama, golongan);
    }

    public int getJamKerja() {
        return jamKerja;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public double hitLembur() {
        int lembur = 0;
        if (jamKerja > 8) {
            lembur = (jamKerja - 8) * 50000;
        }
        return lembur;
    }

    @Override
    public double hitungGatot() {
        gajiTotal = hitGajiPokok() + hitLembur();
        return gajiTotal;
    }

    @Override
    public boolean isSelesai() {
        if (golongan > 1) {
            return true;
        } else{
            return false;
        }
    }
}
