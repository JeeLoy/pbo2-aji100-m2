/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pegawai;

import Pendapatan.Pendapatan;

/**
 *
 * @author User pc
 */
public abstract class Pegawai implements Pendapatan {

    protected String nip;
    protected String nama;
    protected int golongan;
    protected double gajiTotal;

    public Pegawai(String nip, String nama, int golongan) {
        this.nip = nip;
        this.nama = nama;
        this.golongan = golongan;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getGolongan() {
        return golongan;
    }

    public void setGolongan(int golongan) {
        this.golongan = golongan;
    }

    public double getGajiTotal() {
        return gajiTotal;
    }

    public void setGajiTotal(double gajiTotal) {
        this.gajiTotal = gajiTotal;
    }

    public double hitGajiPokok() {
        double gajiPokok = 0;
        if (golongan == 1) {
            gajiPokok = 500000;
        } else if (golongan == 2) {
            gajiPokok = 750000;
        } else if (golongan == 3) {
            gajiPokok = 1000000;
        }
        return gajiPokok;
    }

}
